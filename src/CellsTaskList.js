import { LitElement, html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsTaskList-styles.js';
import aditionalStyles from './aditionalStyles.js';
import '@bbva-web-components/bbva-web-form-search/bbva-web-form-search.js';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default';
import '@bbva-web-components/bbva-web-template-modal/bbva-web-template-modal';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text';
import '@bbva-web-components/bbva-web-form-date/bbva-web-form-date';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-task-list></cells-task-list>
```

##styling-doc

@customElement cells-task-list
*/
export class CellsTaskList extends LitElement {
  static get is() {
    return 'cells-task-list';
  }

  // Declare properties
  static get properties() {
    return {
      tasks: Array,
      tasksTmp: Array,
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.tasks = [];
    this.tasksTmp = [];
  }

  static get styles() {
    return [
      styles,
      aditionalStyles,
      getComponentSharedStyles('cells-task-list-shared-styles'),
    ];
  }

  addTask() {
    this.shadowRoot.querySelector('bbva-web-template-modal').open();
  }

  addNewTask() {
    const inputs = this.shadowRoot.querySelectorAll('.input-task');
    let task = {};
    inputs.forEach((input) => {
      task[input.name] = input.value;
    });
    let tmp = [...this.tasks];
    if(task.title) {
      this.tasks = []
      tmp.unshift(task);
      this.tasks = tmp;
    }
    this.shadowRoot.querySelector('bbva-web-template-modal').close();
  }

  reset() {
    const inputs = this.shadowRoot.querySelectorAll('.input-task');
    inputs.forEach((input) => {
      input.value = '';
    });
  }

  filterTask() {
    let query = this.shadowRoot.querySelector('bbva-web-form-search').value || '';
    query = query.toLowerCase();
      const items = this.shadowRoot.querySelectorAll('.col-section');
      items.forEach(item => {
        console.log('query', query);
        if(query.length > 0 && item.dataset && item.dataset.query && item.dataset.query.toLowerCase().indexOf(query) < 0) {
          item.style.display = 'none';
        } else {
          item.style.display = 'block';
        }
      });
  }

  // Define a template
  render() {
    return html`
      <header>Lista de mis tareas</header>
      <main>
        <section class="toolbar">
          <bbva-web-button-default @click="${this.addTask}"
            >Agregar Tarea</bbva-web-button-default
          >
          <bbva-web-form-search @input="${this.filterTask}" label="Buscar"></bbva-web-form-search>
        </section>
        <section class="task-list-body">
          <div class="row">
            ${this.tasks.map(
              (task) => html`
                <div
                  data-query="${task.title}"
                  class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4	col-xxl-4 col-section"
                >
                  <article>
                    <h2>${task.title}</h2>
                    <div>
                      <span>Inicio: ${task.createDate}</span
                      ><span>Entrega: ${task.endDate}</span>
                    </div>
                  </article>
                </div>
              `
            )}
            ${this.tasks.length === 0
              ? html`<div class="data-empty">Sin tareas para mostrar</div>`
              : html``}
          </div>
        </section>
      </main>

      <bbva-web-template-modal
        heading="Agregar Tarea"
        button="Agregar"
        link="Cancelar"
        @button-click="${this.addNewTask}"
        @link-click="${() => this.shadowRoot.querySelector('bbva-web-template-modal').close()}"
        @visible-changed="${this.reset}"
      >
        <bbva-web-form-text
          class="input-task"
          name="title"
          label="Título de la tarea"
        ></bbva-web-form-text>
        <bbva-web-form-date
          class="input-task"
          name="createDate"
          label="Fecha de incio"
        ></bbva-web-form-date>
        <bbva-web-form-date
          class="input-task"
          name="endDate"
          label="Fecha de finalización"
        ></bbva-web-form-date>
      </bbva-web-template-modal>
    `;
  }
}
