/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`:host {
  display: block;
  box-sizing: border-box;
  --bg-header: #072146;
  --text-color-header: #fff;
  --color-border-list: #1264A5;
}

:host([hidden]), [hidden] {
  display: none !important;
}

*, *:before, *:after {
  box-sizing: inherit;
}

header {
  background-color: var(--bg-header);
  color: var(--text-color-header);
  padding: 15px;
  display: flex;
  align-items: center;
  justify-content: center;
}

main .toolbar {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
main .toolbar bbva-web-form-search {
  width: calc(100% - 163px);
}
main .toolbar bbva-web-button-default {
  background-color: #006C6C;
}
@media only screen and (max-width: 320px) {
  main .toolbar {
    width: 100%;
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: column;
  }
  main .toolbar bbva-web-button-default {
    width: 100%;
    max-width: 100%;
  }
  main .toolbar bbva-web-form-search {
    width: 100%;
  }
}
main .task-list-body {
  padding: 15px;
}
main .task-list-body article {
  padding: 10px;
  border: 1px solid #e1e1e1;
  background-color: #F4F4F4;
  border-left: 3px solid var(--color-border-list);
}
main .task-list-body article h2 {
  font-size: 0.85em;
  margin: 0;
  margin-bottom: 5px;
}
main .task-list-body article div {
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-size: 0.75em;
}
main .task-list-body article div span {
  font-style: italic;
}
main .task-list-body .col-section {
  margin-bottom: 15px;
}
main .task-list-body .data-empty {
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 20px;
}

bbva-web-template-modal bbva-web-form-text, bbva-web-template-modal bbva-web-form-date {
  text-align: left;
  margin-bottom: 10px;
  min-width: 290px;
}

@media only screen and (max-width: 320px) {
  .modal {
    width: 100%;
  }
}
`;